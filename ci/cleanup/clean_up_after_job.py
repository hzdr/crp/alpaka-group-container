#!/usr/bin/env python3

import gitlab
import os
import argparse
from typing import List


def exit_error(text: str):
    """Prints error message and exits application with error code 1.

    Args:
        text (str): Error message.
    """
    print(f"\033[0;31m[ERROR]: {text}\033[0m")
    exit(1)


def clean_up_empty_repos(gitlab_project, tags_to_delete: set[str], dry_run: bool):
    """Remove all image entries, where no tags left or when only the latest tag
    left.

    Repositories are not automatically deleted, when the last
    tag was deleted.

    Args:
        gitlab_project (Project): Project from the gitlab library
        tags_to_delete (set[str]): Set of tags, which should be delete. Is required to
            calculate which repository should be deleted
        dry_run (bool): If True display only which image should be deleted
    """
    repositories = gitlab_project.repositories.list(get_all=True)
    if not isinstance(repositories, list):
        repositories = [repositories]

    if dry_run:
        print(
            "the following images have no tags or only the `latest` tag and would be deleted: "
        )
    else:
        print(
            "the following images have no tags or only the `latest` tag and will be deleted: "
        )

    repos_to_delete = []
    for repo in repositories:
        # if we delete the tags, we can ask the registry for tags which are left
        # if not, we need to calculate the left tags by ourself
        if not dry_run:
            if len(repo.tags.list()) == 0 or (
                len(repo.tags.list()) == 1
                and repo.tags.list()[0].attributes["name"] == "latest"
            ):
                print(f' {repo.attributes["name"]}')
                repos_to_delete.append(repo.get_id())
        else:
            left_tags = (
                set([t.attributes["name"] for t in repo.tags.list()]) - tags_to_delete
            )
            if len(left_tags) == 0 or (
                len(left_tags) == 1 and list(left_tags)[0] == "latest"
            ):
                print(f' {repo.attributes["name"]}')

    # delete repositories afterwards because I'm not sure, what happen if I delete
    # repositories during I loop over they
    for repo_id in repos_to_delete:
        gitlab_project.repositories.delete(repo_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Cleanup a GitLab container registry by a given image tag."
    )

    parser.add_argument(
        "--url",
        type=str,
        help="Set server url. If the argument is not set, read from environment variable CI_SERVER_URL.",
    )

    parser.add_argument(
        "--id",
        type=str,
        help="Set project id. If the argument is not set, read from environment variable CI_PROJECT_ID.",
    )

    parser.add_argument(
        "--token",
        type=str,
        help="Set project access token. If the argument is not set, read from environment variable PRIVATE_JOB_TOKEN.",
    )

    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Show only, which tags should be deleted.",
    )

    parser.add_argument(
        "--print-tags", action="store_true", help="Show all tags from the registry."
    )

    parser.add_argument("--delete-tag", type=str, help="Delete the tag for each image.")

    parser.add_argument(
        "--keep-tags",
        nargs="+",
        help="Delete all tags for each image, which does not match the tags.",
    )

    parser.add_argument(
        "-v",
        action="store_true",
        help="Verbose output",
    )

    args = parser.parse_args()

    if args.url:
        server_url = args.url
    else:
        if not "CI_SERVER_URL" in os.environ:
            exit_error(
                "Server URL is not set via --url argument or CI_SERVER_URL environment variable"
            )
        else:
            server_url = os.environ["CI_SERVER_URL"]

    if args.id:
        project_id = args.id
    else:
        if not "CI_PROJECT_ID" in os.environ:
            exit_error(
                "Project ID is not set via --id argument or CI_PROJECT_ID environment variable"
            )
        else:
            project_id = os.environ["CI_PROJECT_ID"]

    # the token is required for deleting tags but it can be also required to get the images from
    # a private repository
    token = ""
    if args.token:
        token = args.token
    else:
        if "REGISTRY_TOKEN" in os.environ:
            token = os.environ["REGISTRY_TOKEN"]

    if (
        int(args.print_tags)
        + int(args.delete_tag is not None)
        + int(args.keep_tags is not None)
        > 1
    ):
        exit_error(
            "It is only allowed to use one of the arguments at the same time: --print-args, delete-tag, keep-tags"
        )

    if (
        ((args.delete_tag is not None) or (args.keep_tags is not None))
        and not args.dry_run
        and not token
    ):
        exit_error(
            "Personal access token is not set via --token argument or REGISTRY_TOKEN environment variable.\n"
            "The personal access token is required for --delete-tag or --keep-tags if not --dry-run is set."
        )

    if args.v:
        print(f"server url: {server_url}")
        print(f"project id: {project_id}")
        if token:
            print("private access token is set")
        else:
            print("private access token is not set")

    if token:
        gl = gitlab.Gitlab(
            url=server_url,
            private_token=token,
        )
        gl.auth()
    else:
        gl = gitlab.Gitlab(
            url=server_url,
        )

    project = gl.projects.get(project_id)

    repositories = project.repositories.list(get_all=True)
    # project.repositories.list can return a single element instead a list
    if not isinstance(repositories, list):
        repositories = [repositories]

    if args.print_tags:
        tag_list: List[str] = []

        for repo in repositories:
            for tag in repo.tags.list():
                if tag.attributes["name"] not in tag_list:
                    tag_list.append(tag.attributes["name"])

        print(f'available tags: {", ".join(tag_list)}')
        exit(0)

    if args.delete_tag:
        tag_name = args.delete_tag

        if args.dry_run:
            print("The following tags would be deleted: ")
        else:
            print("Delete the following images: ")

        for repo in repositories:
            for tag in repo.tags.list():
                tags_to_delete : List[str] = []
                if tag.attributes["name"] == tag_name:
                    tags_to_delete.append(tag.attributes["name"])

                # delete tags afterwards because I'm not sure, what happen if I delete
                # tags during I loop over they
                for tag in tags_to_delete:
                    print(f'  {repo.attributes["name"]}:{tag}')
                    if not args.dry_run:
                        repo.tags.delete(id=tag)

        clean_up_empty_repos(project, set([tag_name]), args.dry_run)
        exit(0)

    if args.keep_tags:
        tag_names = args.keep_tags

        if args.dry_run:
            print("The following tags would be deleted: ")
        else:
            print("Delete the following images: ")

        all_tags_to_delete: set[str] = set()
        for repo in repositories:
            for tag in repo.tags.list():
                tags_to_delete: List[str] = []
                if tag.attributes["name"] not in tag_names:
                    tags_to_delete.append(tag.attributes["name"])
                    all_tags_to_delete.add(tag.attributes["name"])

                # delete tags afterwards because I'm not sure, what happen if I delete
                # tags during I loop over they
                for tag in tags_to_delete:
                    print(f'  {repo.attributes["name"]}:{tag}')
                    if not args.dry_run:
                        repo.tags.delete(id=tag)

        clean_up_empty_repos(project, all_tags_to_delete, args.dry_run)
        exit(0)
