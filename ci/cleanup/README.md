# About

The `clean_up_after_job.py` allows to delete all tags in a container registry by a given tag or delete all tags, which don't match a list of tags.

# preparation

The clean up scripts needs some preparation in the GitLab repository. Please do the following instructions:

1. Go to your fork of the project on GitLab.com
2. Create a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) (`Settings->Access Tokens`) with the role `Maintainer` and permission `api`, `read_registry` and `write_registry`.
3. Set the token as [variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) with name `REGISTRY_TOKEN` in the CI (`Settings->CI/CD`). Don't forget to set Mask variable flag for security reasons.

# clean_up_after_job.py

The script is executed on the `master` branch after on each push, which includes merged Pull Request.

For testing purpose create a project access token and use the following arguments or setup the following environment variables, before you execute the script.

* **--url** or **CI_SERVER_URL**: URL of the GitLab instance. Normally https://codebase.helmholtz.cloud
* **--token** or **REGISTRY_TOKEN**: Create a temporary project access. Go to `Settings->Access` Tokens 
* **--id** or **CI_PROJECT_ID**: ID of the main project (4742) or you fork. Go to `Settings->General`

To get all options, please run `python clean_up_after_job.py --help`.
